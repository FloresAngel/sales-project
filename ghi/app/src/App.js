import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';
import CustomerListForm  from './components/CustomerListForm'; //Customer form
import CustomerNewForm from './components/CustomerNewForm';
import SalesListForm from './components/SalesListForm'; //Sales Form
import SalesForm from './components/SalesForm';
import SalesPersonListForm from './components/SalesPersonListForm';//sales person Form
import SalesPersonNewForm from './components/SalesPersonNewForm';
import AutomobileForm from './components/AutomobileForm'
import SalesHistory from './components/SalesHistoryForm'
import ManufactureForm from './components/ManufactureForm';
import ModelForm from './components/ModelForm'


function App() {
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />    
          <Route path="/cars" element={<AutomobileForm />} />      
          <Route path="/customers" element={<CustomerListForm />} />    
          <Route path="/customers/new" element={<CustomerNewForm />} />      
          <Route path="/salesperson" element={<SalesPersonListForm />} /> 
          <Route path="/salesperson/new" element={<SalesPersonNewForm />} /> 
          <Route path="/salesperson/history" element={<SalesHistory/>} />          
          <Route path="/sales" element={<SalesListForm/>} />
          <Route path="/sales/new" element={<SalesForm/>} />
          <Route path="/manufacturers" element={<ManufactureForm/>} />
          <Route path="/models" element={<ModelForm/>} />
        </Routes>      
      </div>
    </BrowserRouter>
  );
}

export default App;
