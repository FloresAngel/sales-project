import React, {useEffect, useState} from 'react';
import { Link } from 'react-router-dom';

function CustomerListForm() {
    const [customerList, setCustomerList] = useState([]);    
    // getting the customer list
    const fetchCustomer = async () => {
        try {
            const url = 'http://localhost:8090/api/customers/';
            const response = await fetch(url);   
            if (response.ok) {//if the connection is success
                const data = await response.json();
                setCustomerList(data.customer);
            } else {
                throw new Error('Failed to fetch customer');
            }
        } catch (error) {
            console.error('Error fetching customer:', error);
        }
    };

    const handleDelete= async (idx) => {
        try {// DELETE method with the Customer id            
            const deleteCustomerId = customerList[idx].id;
            const url = `http://localhost:8090/api/customers/${deleteCustomerId}/`;            
            const response = await fetch(url, { method: 'DELETE' });
            if (response.ok) {
                // Remove the deleted Customer from the customerList
                setCustomerList(prevCustomers => prevCustomers.filter(customer => customer.id !== deleteCustomerId));
            } else {
                throw new Error('Failed to delete Customer');
            }
        } catch (error) {
            console.error('Error deleting Customer:', error);
        }
    };

    useEffect(() => {
        fetchCustomer();// Fetch Customer data when component mounts for one time
    }, []);

    return (
        <div className="customers-List">  
        <h1>Customer List</h1>
            <Link to="/customers/new">Add New➕</Link>  
            <div className="customers-Detail">
                <table className="table table-striped">
                    <thead>
                        <tr>
                        <th>ID</th>
                        <th>First Name</th>
                        <th>Last Name</th>
                        <th>Address</th>
                        <th>Phone Number</th>
                        </tr>
                    </thead>
                <tbody>
                    {customerList.map((customer, idx) => {
                        return (
                                <tr key={customer.id}>
                                <td>{customer.id}</td>
                                <td>{ customer.first_name }</td>
                                <td>{ customer.last_name }</td>
                                <td>{ customer.address }</td>
                                <td>{ customer.phone_number }</td>           
                                <td><button onClick={() => handleDelete(idx)} type="button">Delete</button></td>
                                </tr>                                
                            );
                        })}
                    </tbody>
                </table>
            </div>
        </div>
    );
}
    export default CustomerListForm;

