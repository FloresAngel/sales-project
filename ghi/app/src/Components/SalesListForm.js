import React, { useEffect, useState } from 'react';
import { Link } from 'react-router-dom';

// Function to add dot in the last three positions of a string of numbers
const addDotInLastThreePositions = str =>
    str.length > 3
      ? str.split('').slice(0, -3).join('') + '.' + str.slice(-3)
      : '0.' + str.padStart(2, '0');

function SalesListForm() {
    const [salesList, setSalesList] = useState([]);
    const [salespersonList, setSalespersonList] = useState([]);
    const [customerList, setCustomerList] = useState([]);
    const [autoVOList, setAutoVOList] = useState([]);

    // Function to fetch AutoVO list
    const fetchAutoVO = async () => {
        try {
            const url = 'http://localhost:8090/api/autoVO/';
            const response = await fetch(url);
            if (response.ok) {
                const data = await response.json();
                setAutoVOList(data.autoVO);
            } else {
                throw new Error('Failed to fetch AutoVO');
            }
        } catch (error) {
            console.error('Error fetching AutoVO:', error);
        }
    };

    // Function to fetch customer list
    const fetchCustomer = async () => {
        try {
            const url = 'http://localhost:8090/api/customers/';
            const response = await fetch(url);
            if (response.ok) {
                const data = await response.json();
                setCustomerList(data.customer);
            } else {
                throw new Error('Failed to fetch customer');
            }
        } catch (error) {
            console.error('Error fetching customer:', error);
        }
    };

    // Function to fetch salesperson list
    const fetchSalesPerson = async () => {
        try {
            const url = 'http://localhost:8090/api/salesperson/';
            const response = await fetch(url);
            if (response.ok) {
                const data = await response.json();
                setSalespersonList(data.salesperson);
            } else {
                throw new Error('Failed to fetch salesperson');
            }
        } catch (error) {
            console.error('Error fetching salesperson:', error);
        }
    };

    // Function to fetch sales list
    const fetchDataSales = async () => {
        try {
            const url = 'http://localhost:8090/api/sales/';
            const response = await fetch(url);
            if (response.ok) {
                const data = await response.json();
                setSalesList(data.sales);
            } else {
                throw new Error('Failed to fetch sales');
            }
        } catch (error) {
            console.error('Error fetching sales:', error);
        }
    };

    useEffect(() => {
        fetchDataSales(); // Fetch sales data when component mounts
        fetchSalesPerson(); // Fetch salesperson data when component mounts
        fetchCustomer(); // Fetch customer data when component mounts
        fetchAutoVO(); // Fetch AutoVO data when component mounts
    }, []);

    // Function to find salesperson by ID
    const findSalesPersonById = (id) => {
        return salespersonList.find(person => person.id === id);
    };

    // Function to find customer by ID
    const findCustomerById = (id) => {
        return customerList.find(customer => customer.id === id);
    };

    // Function to find AutoVO by ID
    const findAutoVOById = (id) => {
        return autoVOList.find(auto => auto.id === id);
    };

    return (
        <div className="salespersons-List">
            <h1>Sales List</h1>
            <Link to="/sales/new">Add New➕</Link>
            <div className="salespersons-Detail">
                <table className="table table-striped">
                    <thead>
                        <tr>
                            <th>Salesperson Employee ID</th>
                            <th>Salesperson Name</th>
                            <th>Customer Name</th>
                            <th>VIN</th>
                            <th>Price</th>
                        </tr>
                    </thead>
                    <tbody>
                        {salesList.map((sale) => {
                            const salesperson = findSalesPersonById(sale.salesperson_id);
                            const customer = findCustomerById(sale.customer_id);
                            const autoVO = findAutoVOById(sale.automobile_id);
                            return (
                                <tr key={sale.id}>
                                    <td>{salesperson ? salesperson.employee_id : '-'}</td>
                                    <td>{salesperson ? `${salesperson.first_name} ${salesperson.last_name}` : '-'}</td>
                                    <td>{customer ? `${customer.first_name} ${customer.last_name}` : '-'}</td>
                                    <td>{autoVO ? autoVO.vin : '-'}</td>
                                    <td>{"$"}{addDotInLastThreePositions(sale.price.toString())}</td>
                                </tr>
                            );
                        })}
                    </tbody>
                </table>
            </div>
        </div>
    );
}

export default SalesListForm;