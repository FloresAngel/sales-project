import React, {useEffect, useState} from 'react';
import { Link } from 'react-router-dom';

function AutomobileForm() {
  const [model, setModel] = useState('');
  const [manufacturer, setManufacturer] = useState('');
  const [models, setModels] = useState([]);

  const handleManufacturerChange = (event) => {
    const selectedManufacturer = event.target.value;
    setManufacturer(selectedManufacturer);
    
    // Fetch models based on the selected manufacturer (You can implement this logic)
    // For now, I'm just providing static models for demonstration
    if (selectedManufacturer === 'Chrysler') {
      setModels(['Sebring', 'Pacifica', '300']);
    } else if (selectedManufacturer === 'Rivian') {
      setModels(['R1T', 'R1S']);
    } else if (selectedManufacturer === 'Toyota') {
      setModels(['Camry', 'Corolla', '4Runner']);
    } else {
      setModels([]);
    }
    setModel('');
  };

  const handleModelChange = (event) => {
    setModel(event.target.value);
  };

  const handleSubmit = (event) => {
    event.preventDefault();
    console.log('Submitted Form:', { model, manufacturer });
    // You can add your logic here to submit the form data
  };

  return (
    <div>
      <h2>Automobile Form</h2>
      <form onSubmit={handleSubmit}>
        <div>
          <label htmlFor="manufacturer">Manufacturer:</label>
          <select
            id="manufacturer"
            value={manufacturer}
            onChange={handleManufacturerChange}
            required
          >
            <option value="">Select Manufacturer</option>
            <option value="Chrysler">Chrysler</option>
            <option value="Rivian">Rivian</option>
            <option value="Toyota">Toyota</option>
            {/* Add more options as needed */}
          </select>
        </div>
        <div>
          <label htmlFor="model">Model:</label>
          <select
            id="model"
            value={model}
            onChange={handleModelChange}
            required
            disabled={!manufacturer} // Disable model select until manufacturer is selected
          >
            <option value="">Select Model</option>
            {models.map((model) => (
              <option key={model} value={model}>
                {model}
              </option>
            ))}
          </select>
        </div>
        <button type="submit" disabled={!model || !manufacturer}>Submit</button>
      </form>
    </div>
  );
}

export default AutomobileForm;
