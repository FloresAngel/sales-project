import React, {useEffect, useState} from 'react';
import { Link } from 'react-router-dom';

function SalespersonListForm() {
    const [salespersonList, setSalespersonList] = useState([]);    
    // getting the salesperson list
    const fetchSalesPerson = async () => {
        try {
            const url = 'http://localhost:8090/api/salesperson/';
            const response = await fetch(url);   
            if (response.ok) {//if the connection is success
                const data = await response.json();
                setSalespersonList(data.salesperson);
            } else {
                throw new Error('Failed to fetch salesperson');
            }
        } catch (error) {
            console.error('Error fetching salesperson:', error);
        }
    };

    const handleDelete= async (idx) => {
        try {// DELETE method with the salesperson id            
            const deleteSalespersonId = salespersonList[idx].id;
            const url = `http://localhost:8090/api/salesperson/${deleteSalespersonId}/`;
            const response = await fetch(url, { method: 'DELETE'});
            if (response.ok) {
                // Remove the deleted salesperson from the salespersonList
                setSalespersonList(prevSalespersons => prevSalespersons.filter(salesperson => salesperson.id !== deleteSalespersonId));
            } else {
                throw new Error('Failed to delete Salesperson');
            }
        } catch (error) {
            console.error('Error deleting Salesperson:', error);
        }
    };

    useEffect(() => {
        fetchSalesPerson();// Fetch salesperson data when component mounts for one time
    }, []);
      

    return (
        <div className="salespersons-List">  
            <h1>Sales Persons List</h1>
            <Link to="/salesperson/new">Add New➕</Link> 
            <div><Link to="/salesperson/history">History 📑</Link> </div>
             
            <div className="salespersons-Detail">
                <table className="table table-striped">
                    <thead>
                        <tr>
                        <th>Employee ID</th>
                        <th>First Name</th>
                        <th>Last Name</th>                  
                        </tr>
                    </thead>
                <tbody>
                    {salespersonList.map((salesperson, idx) => {
                        return (
                                <tr key={salesperson.id}>
                                <td>{salesperson.employee_id}</td>
                                <td>{ salesperson.first_name }</td>
                                <td>{ salesperson.last_name }</td>          
                                <td><button onClick={() => handleDelete(idx)} type="button">Delete</button></td>
                                </tr>                                
                            );
                        })}
                    </tbody>
                </table>
            </div>
        </div>
    );
}
    export default SalespersonListForm;