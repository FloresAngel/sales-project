import React, { useState } from 'react';
import { useNavigate } from 'react-router-dom';

function SalesPersonNewForm() {
    const [firstname, setFirstName] = useState('');
    const [lastname, setLastName] = useState('');
    const [employeeid, setEmployeeID] = useState('');
    const navigate = useNavigate();

    const handleFirstnameChange = (event) => {
        setFirstName(event.target.value);
    };

    const handleLastNameChange = (event) => {
        setLastName(event.target.value);
    };

    const handleEmployeeIDChange = (event) => {
        setEmployeeID(event.target.value);
    };

    const handleSubmit = async (event) => {
        event.preventDefault();
        const data = {
            first_name: firstname,
            last_name: lastname,            
            employee_id: `${firstname.substring(0,2).toUpperCase()}${lastname.substring(0,2).toUpperCase()}${employeeid}`, 
        };
        const customerUrl = 'http://localhost:8090/api/salesperson/';
        const fetchConfig = {
            method: "POST",
            body: JSON.stringify(data),
            headers: {
              'Content-Type': 'application/json',
            },
          };
        
        const response = await fetch(customerUrl, fetchConfig);
        try {
            
            if (response.ok) {
                setFirstName('');
                setLastName('');
                setEmployeeID('');  
                navigate(-1);
            } else {
                throw new Error('Customer creation failed');
            }
        } catch (error) {
            console.error('Error in Creating Customer:', error);
        }
    };

    return (
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1>Create new Sales Person</h1>
                    <form onSubmit={handleSubmit}>
                        <div className="form-floating mb-3">
                            <input value={firstname} onChange={handleFirstnameChange} placeholder="First Name" required
                                type="text" name="first_name" id="first_name" className="form-control" />
                            <label htmlFor="first_name">First Name</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input value={lastname} onChange={handleLastNameChange} placeholder="Last Name" required
                                type="text" name="last_name" id="last_name" className="form-control" />
                            <label htmlFor="last_name">Last Name</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input value={employeeid} onChange={handleEmployeeIDChange} placeholder="Employee ID" required
                                type="text" name="employeeid" id="employeeid" className="form-control" />
                            <label htmlFor="employeeid">Employee ID</label>
                        </div>
                        <button type="submit" className="btn btn-primary">Create</button>
                    </form>
                </div>
            </div>
        </div>
    );
}

export default SalesPersonNewForm;