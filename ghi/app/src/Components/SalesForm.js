import React, { useState, useEffect } from 'react';
import { useNavigate } from 'react-router-dom';

function SalesForm() {
  const [autoVO, setAutoVO] = useState('');  
  const [salesperson, setSalesPerson] = useState('');
  const [customer, setCustomer] = useState('');
  const [price, setPrice] = useState('');
  const [autoVOList, setAutoVOList] = useState([]); // Option tag variable
  const [salespersons, setSalespersons] = useState([]);
  const [customers, setCustomers] = useState([]);

  const navigate = useNavigate();
  

  // Function to fetch AutoVO list
  const fetchAutoVO = async () => {
      try {
          const url = 'http://localhost:8090/api/autoVO/';
          const response = await fetch(url);
          if (response.ok) {
              const data = await response.json();
              setAutoVOList(data.autoVO);
          } else {
              throw new Error('Failed to fetch AutoVO');
          }
      } catch (error) {
          console.error('Error fetching AutoVO:', error);
      }
  };

  // Fetching the salesperson list
  const fetchSalespersons = async () => {
    try {
      const url = 'http://localhost:8090/api/salesperson/';
      const response = await fetch(url);
      if (response.ok) {
        const data = await response.json();
        setSalespersons(data.salesperson);
      } else {
        throw new Error('Failed to fetch salespersons');
      }
    } catch (error) {
      console.error('Error fetching salespersons:', error);
    }
  };

  // Fetching the customer list
  const fetchCustomers = async () => {
    try {
      const url = 'http://localhost:8090/api/customers/';
      const response = await fetch(url);
      if (response.ok) {
        const data = await response.json();
        setCustomers(data.customer);
      } else {
        throw new Error('Failed to fetch customers');
      }
    } catch (error) {
      console.error('Error fetching customers:', error);
    }
  };

  useEffect(() => {   
    fetchSalespersons();
    fetchCustomers();
    fetchAutoVO(); // Fetch AutoVO data when component mounts
  }, []);

  const handleAutoVOChange = (event) => {
    setAutoVO(event.target.value);
  };

  const handleSalesPersonChange = (event) => {
    setSalesPerson(event.target.value);
  };

  const handleCustomerChange = (event) => {
    setCustomer(event.target.value);
  };

  const handlePriceChange = (event) => {
    setPrice(event.target.value);
  };

  const handleSubmit = async (event) => {
    event.preventDefault();
    const data = { automobile_id: autoVO , salesperson_id: salesperson, customer_id: customer, price };
    const url = 'http://localhost:8090/api/sales/';
    const fetchConfig = {
      method: 'POST',
      body: JSON.stringify(data),
      headers: {
        'Content-Type': 'application/json',
      },
    };
    
    const response = await fetch(url, fetchConfig);
    try {      
      // console.log("response", data);
      if (response.ok) {
        // Set sold field of selected autoVO to true
        const updatedAutoVO = { ...autoVOList.find(item => item.id === autoVO), sold: true };
        const updateAutoVOUrl = `http://localhost:8090/api/autoVO/${autoVO}/`;
        const updateAutoVOConfig = {
          method: 'PUT',
          body: JSON.stringify(updatedAutoVO),
          headers: {
            'Content-Type': 'application/json',
          },
        };
        const updateAutoVOResponse = await fetch(updateAutoVOUrl, updateAutoVOConfig);
        
      if (updateAutoVOResponse.ok) {
        // Reset form fields
        setAutoVOList([]);
        setSalesPerson('');
        setCustomer('');
        setPrice('');
        
        navigate(-1);
      } else {
        throw new Error('Failed to update autoVO');
      }
    } else {
      throw new Error('Sales creation failed');
    }
  } catch (error) {
    console.error('Error in Creating Sales:', error);
  }
};

  return (
    <div className="container mt-5">
      <div className="row justify-content-center">
        <div className="col-lg-6">
          <div className="card shadow p-4">
            <h1 className="mb-4">Create new Sales</h1>
            <form onSubmit={handleSubmit}>

              <div className="mb-3">
                <select onChange={handleAutoVOChange} required name="autoVO" id="autoVO" className="form-select">
                  <option value="">Choose a VIN</option>
                  {autoVOList.map(autoVO => (
                    <option key={autoVO.id} value={autoVO.id}>{autoVO.vin}</option>
                  ))}
                </select>
              </div>

              <div className="mb-3">
                <select onChange={handleSalesPersonChange} required name="salesperson" id="salesperson" className="form-select">
                  <option value="">Choose a Sales Person</option>
                  {salespersons.map(salesperson => (
                    <option key={salesperson.id} value={salesperson.id}>{salesperson.last_name} {salesperson.first_name}</option>
                  ))}
                </select>
              </div>

              <div className="mb-3">
                <select onChange={handleCustomerChange} required name="customer" id="customer" className="form-select">
                  <option value="">Choose a customer</option>
                  {customers.map(customer => (
                    <option key={customer.id} value={customer.id}>{customer.last_name} {customer.first_name}</option>
                  ))}
                </select>
              </div>

              <div className="mb-3">
                <input value={price} onChange={handlePriceChange} placeholder="Price" required type="number" min="1" name="price" id="price" className="form-control" />
              </div>

              <button type="submit" className="btn btn-primary">Create</button>
            </form>
          </div>
        </div>
      </div>
    </div>
  );
}

export default SalesForm;