import React, { useState, useEffect } from 'react';

function ModelForm() {
    const [name, setName] = useState('');
    const [pictureUrl, setPictureUrl] = useState('');
    const [manufacturersList, setManufacturersList] = useState([]);
    const [selectedManufacturer, setSelectedManufacturer] = useState('');

    // Fetching the manufacturers list
    const fetchManufacturers = async () => {
        try {
            const ManufacturersUrl = 'http://localhost:8100/api/manufacturers/';
            const response = await fetch(ManufacturersUrl);
            if (response.ok) {
                const data = await response.json();
                setManufacturersList(data.manufacturers);
            } else {
                throw new Error('Failed to fetch manufacturers');
            }
        } catch (error) {
            console.error('Error fetching manufacturers:', error);
        }
    };

    const handleNameChange = (event) => {
        setName(event.target.value);
    };
    const handlePictureUrlChange = (event) => {
        setPictureUrl(event.target.value);
    };
    const handleManufactureChange = (event) => {
        setSelectedManufacturer(event.target.value);
    };

    const handleSubmit = async (event) => {
        event.preventDefault();
        const data = { name, picture_url: pictureUrl, manufacturer_id: +selectedManufacturer };
        const ModelUrl = 'http://localhost:8100/api/models/';
        const fetchConfig = {
            method: 'POST',
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };

        try {
            const response = await fetch(ModelUrl, fetchConfig);
            console.log("response", data);
            if (response.ok) {
                setName('');
                setPictureUrl('');
                setSelectedManufacturer([] );
                // Redirect to the previous page or handle navigation as needed
            } else {
                throw new Error('Model creation failed');
            }
        } catch (error) {
            console.error('Error in Creating Model:', error);
        }
    };

    useEffect(() => {   
        fetchManufacturers();        
    }, []);

    return (
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1>Create new Car Model</h1>
                    <form onSubmit={handleSubmit}>
                        <div className="form-floating mb-3">
                            <input
                                value={name}
                                onChange={handleNameChange}
                                placeholder="Name"
                                required
                                type="text"
                                name="name"
                                id="name"
                                className="form-control"
                            />
                            <label htmlFor="name">Name</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input
                                value={pictureUrl}
                                onChange={handlePictureUrlChange}
                                placeholder="pictureurl"
                                required
                                type="text"
                                name="picture_url"
                                id="pictureu_url"
                                className="form-control"
                            />
                            <label htmlFor="pictureurl">Picture URL</label>
                        </div>
                        <div className="mb-3">
                            <select onChange={handleManufactureChange} required name="manufacturerid" id="manufacturerid" className="form-select">
                                <option value="">Choose a Manufacturer</option>
                                {manufacturersList.map(manufacturer => (
                                    <option key={manufacturer.id} value={manufacturer.id}>{manufacturer.name}</option>
                                ))}
                            </select>
                        </div>
                        <button type="submit" className="btn btn-primary">
                            Create
                        </button>
                    </form>
                </div>
            </div>
        </div>
    );
}

export default ModelForm;
