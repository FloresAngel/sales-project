import django
import os
import sys
import time
import json
import requests

sys.path.append("")
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "sales_project.settings")
django.setup()

# Import models from sales_rest, here.
# from sales_rest.models import Something
from sales_rest.models import AutomobileVO

def get_automobile_inventory():
    url= "http://inventory-api:8000/api/automobiles/" 
    response = requests.get(url)
    # Parse the JSON response
    content= json.loads(response.content)
     # Iterate over the VINs obtained from the response
    for automobile_data in content["autos"]:#using the relatename from the model Automobile
    # Update or create AutomobileVO objects based on VIN data
        AutomobileVO.objects.update_or_create(   
             # creating the object from the model       
                vin = automobile_data["vin"],           
           #any other model that i want to pass
            defaults = {
                "sold":automobile_data["sold"],                      
            }
        )

def poll():
    while True:
        print('Sales poller polling for data')
        try:
           # Call the update_inventory function
            get_automobile_inventory()            
        except Exception as e:
            print(e, file=sys.stderr)
        time.sleep(60)

if __name__ == "__main__":
    poll()
