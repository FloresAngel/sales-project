from django.db import models
# from django.urls import reverse

# Create your models here.
class Salesperson(models.Model):
    first_name = models.CharField(max_length=50)
    last_name = models.CharField(max_length=50)
    employee_id = models.CharField(max_length=20)

class Customer(models.Model):
    first_name = models.CharField(max_length=50)
    last_name = models.CharField(max_length=50)
    address = models.CharField(max_length=100)
    phone_number = models.CharField(max_length=20)

class AutomobileVO(models.Model):
    vin = models.CharField(max_length=17)
    sold = models.BooleanField(default=False)

    def __str__(self):
        return self.vin
    
class Sale(models.Model):
    automobile = models.ForeignKey(
        'AutomobileVO',
        on_delete=models.CASCADE,
        related_name= "sale"
    )
    salesperson = models.ForeignKey(
        Salesperson,
        on_delete=models.CASCADE,
        related_name="sale"
    )
    customer = models.ForeignKey(
        Customer,
        on_delete=models.CASCADE,
        related_name= "sale"
    )
    price = models.DecimalField(max_digits=10, decimal_places=2)

