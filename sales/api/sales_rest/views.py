from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
from common.json import ModelEncoder
import json
from .models import Salesperson, Customer, Sale, AutomobileVO
from decimal import Decimal

# Create your views here.

class AutomobileVOEncoder(ModelEncoder):
    model = AutomobileVO
    properties = ["id","vin","sold"]

class SaleEncoder(ModelEncoder):
    model= Sale
    def default(self, obj):
        if isinstance(obj, Decimal):
            return float(obj)  # Convert Decimal to float
        elif isinstance(obj, Sale):
            # Custom serialization logic for Sale model
            return {
                'id': obj.id,
                'price': float(obj.price),  # Convert Decimal price to float
                'automobile': obj.automobile.id,
                'salesperson': obj.salesperson.id,
                'customer': obj.customer.id
            }
        return super().default(obj)  
    # properties = ["id","price","automobile","salesperson","customer",]

class CustomerEncoder(ModelEncoder):
    model = Customer
    properties = ["id", "first_name", "last_name", "address", "phone_number"]

class SalesPersonEncoder(ModelEncoder):
    model= Salesperson
    properties = ["id","first_name", "last_name", "employee_id"]


    encoders = {
        "automobiles" : AutomobileVOEncoder(),
    }


@require_http_methods(["GET", "POST","DELETE"])
def api_salesperson(request,pk=None):
    if request.method == "GET":
        sales_personObject = list(Salesperson.objects.all().values())
        return JsonResponse(
            {"salesperson": sales_personObject},
            encoder=SalesPersonEncoder,
            safe=False
        )    
    elif request.method == "DELETE":
        try:
            sales_personObject = Salesperson.objects.get(id=pk)
            sales_personObject.delete()
            return JsonResponse({"message": "Salesperson deleted successfully"})
        except Salesperson.DoesNotExist:
            return JsonResponse({"message": "Salesperson does not exist"}, status=404)      
    else: #request.method == "POST":       
        try:
            content = json.loads(request.body)      
            sales_personObject = Salesperson.objects.create(**content)
            return JsonResponse(
                sales_personObject,
                encoder=SalesPersonEncoder,
                safe=False,
            )
        except Exception as e:
            response = JsonResponse(
                {"message": "Could not create Salesperson", "error": str(e)}
            )
            response.status_code = 400
            return response                


@require_http_methods(["GET", "POST","DELETE"])
def api_customer(request,pk=None):
    if request.method == "GET":
        customer_Object= Customer.objects.all()
        return JsonResponse(
            {"customer": customer_Object},
            encoder = CustomerEncoder,
        )
    elif request.method == "DELETE":
        try:
            customer_object = Customer.objects.get(id=pk)
            customer_object.delete()
            return JsonResponse(
                {"message": "Customer deleted successfully"}
            )
        except Customer.DoesNotExist:
            return JsonResponse({"message": "Customer does not exist"})       
    else: #request.method == "POST":       
        try:
            content = json.loads(request.body)      
            customer_Object = Customer.objects.create(**content)            
            return JsonResponse(
                customer_Object,
                encoder=CustomerEncoder,
                safe=False,
            )
        except Exception as e:
            response = JsonResponse(
                {"message": "Could not create Salesperson", "error": str(e)}
            )
            response.status_code = 400
            return response    


@require_http_methods(["GET", "POST", "PUT", "DELETE"])
def api_sales(request,pk=None):
    if request.method == 'GET':  
        sales_Object = list(Sale.objects.all().values())
        return JsonResponse(
            {"sales": sales_Object},           
            encoder=SaleEncoder,
              safe=False,
              )    
    elif request.method == 'POST':
        try:
            content = json.loads(request.body)      
            sales_Object = Sale.objects.create(**content)            
            return JsonResponse(
                sales_Object,
                encoder=SaleEncoder,
                safe=False,
            )
        except Exception as e:
            response = JsonResponse(
                {"message": "Could not create Salesperson", "error": str(e)}
            )
            response.status_code = 400
            return response   
    elif request.method == 'DELETE':
        if pk is not None:
            try:
                sale_Object = Sale.objects.get(id=pk)
                sale_Object.delete()
                return JsonResponse(
                    {"message": "Sale deleted successfully"},
                    status=200,
                )
            except Sale.DoesNotExist:
                return JsonResponse(
                    {"error": "Sale not found"},
                    status=404,
                )
        else:
            return JsonResponse(
                {"error": "No primary key provided in the request"},
                status=400,
            )
# ******************
    # elif request.method == "DELETE":
    #     try:
    #         sale_Object = Sale.objects.get(id=pk)
    #         sale_Object.delete()            
    #         return JsonResponse(
    #             SaleObject,
    #             encoder=SaleEncoder,
    #             safe=False,
    #         )
    #     except Customer.DoesNotExist:
    #         return JsonResponse({"message": "Does not exist"})  



@require_http_methods(["GET", "PUT"])
def api_automobileVOlist (request,pk=None):
    if request.method == 'GET':  
        AutoVO_Object = list(AutomobileVO.objects.all().values())
        return JsonResponse(
            {"autoVO": AutoVO_Object},           
            encoder=SaleEncoder,
            safe=False,
        )   
    else:  # PUT method to update an existing AutomobileVO object
        if pk is not None:
            try:
                autoVO_Object = AutomobileVO.objects.get(id=pk)
                data = json.loads(request.body)
                if 'sold' in data:
                    # Update the 'sold' field of the AutomobileVO object
                    autoVO_Object.sold = data['sold']
                    # Update the 'vin' field of the AutomobileVO object if provided in the request data
                    autoVO_Object.vin = data.get('vin', autoVO_Object.vin)
                    autoVO_Object.save()
                    # Return a success message in the JSON response
                    return JsonResponse(
                        {"message": "AutomobileVO updated successfully"},
                        status=200,
                    )
                else:
                    return JsonResponse(
                        {"error": "No 'sold' field provided in the request data"},
                        status=400,
                    )
            except AutomobileVO.DoesNotExist:
                return JsonResponse(
                    {"error": "AutomobileVO not found"},
                    status=404,
                )
        else:
            return JsonResponse(
                {"error": "No primary key provided in the request"},
                status=400,
            )